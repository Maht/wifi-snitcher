# Wi-Fi Snitcher

This Rust application retrieves and parses Wi-Fi profiles on a Windows machine using the Windows API. It lists available Wi-Fi interfaces and their associated profiles, printing out the profile name, authentication type, and password (if available).

## Features

- Retrieves available Wi-Fi interfaces using the Windows WLAN API.
- Lists Wi-Fi profiles associated with each interface.
- Parses and extracts authentication types and passwords from the profile's XML data.

## Requirements

- Windows operating system
- Rust toolchain installed
- Required dependencies:
  - `windows` crate for interacting with Windows APIs.

## Usage

### 1. Clone the repository

```bash
git clone https://gitlab.com/Maht/wifi-snitcher
cd wifi-snitcher
```

### 2. Install dependencies

Add the following dependencies to your `Cargo.toml`:

```toml
[dependencies]
"Data_Xml_Dom",
"Win32_Foundation",
"Foundation_Collections",
"Win32_NetworkManagement_WiFi",
"Win32_Security",
"Win32_System_Com",
"Win32_System_Threading",
"Win32_UI_WindowsAndMessaging",
```

### 3. Build the project

On development mode:

```bash
cargo build
```

On release mode:

```bash
cargo build --release
```

### 4. Run the application

```bash
cargo run
```

## Example Output

When you run the program, it will display information about your Wi-Fi interfaces and profiles:

```bash
Interface Description: Wi-Fi
Wi-Fi Name: HomeNetwork, Authentication: WPA2PSK
Password: mysecretpassword

Wi-Fi Name: GuestNetwork, Authentication: open
No password
```

## Functions

- `open_wlan_handle`: Opens a handle to the WLAN API.
- `close_wlan_handle`: Closes the WLAN API handle.
- `enum_wlan_interfaces`: Enumerates available WLAN interfaces.
- `grab_interface_profiles`: Retrieves the list of profiles for a specific interface.
- `get_profile_xml`: Gets the XML configuration of a Wi-Fi profile.
- `load_xml_data`: Loads XML data into a `XmlDocument` object.
- `traverse_xml_tree`: Traverses the XML tree to extract specific information (e.g., authentication type, password).
- `parse_utf16_slice`: Converts a slice of UTF-16 data to a `OsString`.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more information.
