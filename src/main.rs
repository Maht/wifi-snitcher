use serde::Deserialize;
use std::{
    error::Error,
    ffi::OsString,
    fs::OpenOptions,
    io::{Result as IoResult, Write},
    os::windows::prelude::*,
    ptr::{null_mut, NonNull},
};
use wmi::{COMLibrary, WMIConnection};
use windows::{
    core::{Error as WinError, GUID, HSTRING, PCWSTR, PWSTR},
    Data::Xml::Dom::{XmlDocument, XmlElement},
    Win32::{
        Foundation::{HANDLE, INVALID_HANDLE_VALUE, WIN32_ERROR},
        NetworkManagement::WiFi::{
            WlanCloseHandle, WlanEnumInterfaces, WlanFreeMemory, WlanGetProfile,
            WlanGetProfileList, WlanOpenHandle, WLAN_API_VERSION_2_0, WLAN_INTERFACE_INFO_LIST,
            WLAN_PROFILE_GET_PLAINTEXT_KEY, WLAN_PROFILE_INFO_LIST,
        },
    },
};

#[derive(Deserialize, Debug)]
struct Win32ComputerSystemProduct {
    UUID: String,
}

fn get_smbios_uuid() -> Result<String, Box<dyn Error>> {
    // Initialize COM library
    let com_con = COMLibrary::new().map_err(|e| format!("COMLibrary error: {}", e))?;
    // Create WMI connection
    let wmi_con = WMIConnection::new(com_con.into())
        .map_err(|e| format!("WMIConnection error: {}", e))?;

    // Execute WQL query to get the UUID
    let results: Vec<Win32ComputerSystemProduct> = wmi_con
        .query()
        .map_err(|e| format!("WMI query error: {}", e))?;

    if let Some(product) = results.first() {
        Ok(product.UUID.clone())
    } else {
        Err("Failed to retrieve SMBIOS UUID via WMI.".into())
    }
}

fn write_to_file(filename: &str, content: &str, append: bool) -> IoResult<()> {
    let mut options = OpenOptions::new();
    options.write(true);

    if append {
        options.append(true);
    } else {
        options.create(true).truncate(true);
    }

    let mut file = options.open(filename)?;
    file.write_all(content.as_bytes())?;
    Ok(())
}

fn open_wlan_handle(api_version: u32) -> Result<HANDLE, WinError> {
    let mut negotiated_version = 0;
    let mut wlan_handle = INVALID_HANDLE_VALUE;

    let result =
        unsafe { WlanOpenHandle(api_version, None, &mut negotiated_version, &mut wlan_handle) };

    WIN32_ERROR(result).ok()?;
    Ok(wlan_handle)
}

fn close_wlan_handle(wlan_handle: HANDLE) -> Result<(), WinError> {
    let result = unsafe { WlanCloseHandle(wlan_handle, None) };
    WIN32_ERROR(result).ok()
}

fn enum_wlan_interfaces(handle: HANDLE) -> Result<NonNull<WLAN_INTERFACE_INFO_LIST>, WinError> {
    let mut interface_ptr = null_mut();

    let result = unsafe { WlanEnumInterfaces(handle, None, &mut interface_ptr) };
    WIN32_ERROR(result).ok()?;

    NonNull::new(interface_ptr).ok_or_else(|| WinError::from_win32())
}

fn grab_interface_profiles(
    handle: HANDLE,
    interface_guid: &GUID,
) -> Result<NonNull<WLAN_PROFILE_INFO_LIST>, WinError> {
    let mut wlan_profiles_ptr = null_mut();

    let result =
        unsafe { WlanGetProfileList(handle, interface_guid, None, &mut wlan_profiles_ptr) };
    WIN32_ERROR(result).ok()?;

    NonNull::new(wlan_profiles_ptr).ok_or_else(|| WinError::from_win32())
}

fn parse_utf16_slice(string_slice: &[u16]) -> Option<OsString> {
    let null_index = string_slice.iter().position(|&c| c == 0)?;
    Some(OsString::from_wide(&string_slice[..null_index]))
}

fn load_xml_data(xml: &str) -> Result<XmlDocument, WinError> {
    let xml_document = XmlDocument::new()?;
    xml_document.LoadXml(&HSTRING::from(xml))?;
    Ok(xml_document)
}

fn traverse_xml_tree(xml: &XmlElement, node_path: &[&str]) -> Option<String> {
    let mut current_node = xml.clone();

    for node_name in node_path {
        let mut found = false;

        let child_nodes = current_node.ChildNodes().ok()?;
        let child_nodes_len = child_nodes.Length().ok()?;

        for i in 0..child_nodes_len {
            let node = child_nodes.Item(i).ok()?;
            if node.NodeName().ok()?.to_string() == *node_name {
                // Cast IXmlNode to XmlElement
                current_node = node.cast::<XmlElement>().ok()?;
                found = true;
                break;
            }
        }

        if !found {
            return None;
        }
    }

    current_node.InnerText().ok().map(|s| s.to_string())
}

fn get_profile_xml(
    handle: HANDLE,
    interface_guid: &GUID,
    profile_name: &str,
) -> Result<String, WinError> {
    let mut profile_xml_data = PWSTR::null();
    let mut flags = WLAN_PROFILE_GET_PLAINTEXT_KEY;

    let result = unsafe {
        WlanGetProfile(
            handle,
            interface_guid,
            PCWSTR(HSTRING::from(profile_name).as_ptr()),
            None,
            &mut profile_xml_data,
            Some(&mut flags),
            None,
        )
    };

    WIN32_ERROR(result).ok()?;

    let xml_string = unsafe { profile_xml_data.to_string()? };
    unsafe { WlanFreeMemory(profile_xml_data.as_ptr().cast()) };
    Ok(xml_string)
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Retrieve the UUID and set the filename
    let uuid = get_smbios_uuid()?;
    let filename = format!("{}.txt", uuid);

    // Initialize output string
    let mut output = String::new();

    let wlan_handle = open_wlan_handle(WLAN_API_VERSION_2_0)?;
    let interface_ptr = enum_wlan_interfaces(wlan_handle)?;

    let interfaces_list = unsafe {
        std::slice::from_raw_parts(
            (*interface_ptr.as_ptr()).InterfaceInfo.as_ptr(),
            (*interface_ptr.as_ref()).dwNumberOfItems as usize,
        )
    };

    for interface_info in interfaces_list {
        let interface_description =
            parse_utf16_slice(&interface_info.strInterfaceDescription).unwrap_or_default();

        output.push_str(&format!(
            "Interface Description: {}\n",
            interface_description.to_string_lossy()
        ));

        let wlan_profile_ptr = grab_interface_profiles(wlan_handle, &interface_info.InterfaceGuid)?;
        let wlan_profile_list = unsafe {
            std::slice::from_raw_parts(
                (*wlan_profile_ptr.as_ptr()).ProfileInfo.as_ptr(),
                (*wlan_profile_ptr.as_ref()).dwNumberOfItems as usize,
            )
        };

        for profile in wlan_profile_list {
            let profile_name = parse_utf16_slice(&profile.strProfileName).unwrap_or_default();
            let profile_name_str = profile_name.to_string_lossy();

            let profile_xml_data =
                get_profile_xml(wlan_handle, &interface_info.InterfaceGuid, &profile_name_str)?;

            let xml_document = load_xml_data(&profile_xml_data)?;
            let root = xml_document.DocumentElement()?;

            if let Some(auth_type) = traverse_xml_tree(
                &root,
                &["MSM", "security", "authEncryption", "authentication"],
            ) {
                output.push_str(&format!(
                    "Wi-Fi Name: {}, Authentication: {}\n",
                    profile_name_str, auth_type
                ));

                if auth_type == "open" {
                    output.push_str("No password\n");
                } else if let Some(password) =
                    traverse_xml_tree(&root, &["MSM", "security", "sharedKey", "keyMaterial"])
                {
                    output.push_str(&format!("Password: {}\n", password));
                }
            }
        }

        unsafe { WlanFreeMemory(wlan_profile_ptr.as_ptr().cast()) };
    }

    unsafe { WlanFreeMemory(interface_ptr.as_ptr().cast()) };
    close_wlan_handle(wlan_handle)?;

    // Write the output to the file
    write_to_file(&filename, &output, false)?;

    Ok(())
}
